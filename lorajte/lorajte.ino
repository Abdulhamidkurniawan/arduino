#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SPI.h>
#include <LoRa.h>
#include <ArduinoJson.h>

#define nss D10
#define rst D14
#define dio0 D2

WiFiClient client;
HTTPClient http;// Wi-Fi

String ssid = "Lab_elektro";
//String ssid = "SUNAR";
String password = "";
String apikey = "QXIM6BXKA9R7VJF0";
String server = "jte.polnes.ac.id";

String WebAddress = "http://jte.polnes.ac.id/public/lora/public/api/";
String writeAPIKey;
String tsfield1Name;
String request_string;

String out;

String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages
byte localAddress = 0x01;     // address of this device Gateway
byte destination = 0x11;      // destination to send to
byte packetLength = 2;        // packet length
byte aType1 = 0x03;           // actuator type (valve)
byte aArg1 = 0x03;            // turn valve close

byte recipient;         // recipient address
byte sender;            // sender address
byte inMsgId;           // incoming msg ID
byte inLength;          // incoming msg length
byte inSType1;          // incoming payload
byte inSValue1;
byte inSType2;
byte inSValue2;
byte inSType3;
byte inSValue3;
byte inSType4;
byte inSValue4;
byte inSTipe;
char c;

long lastSendTime = 0;        // last send time
int interval = 5000;          // interval between sends

void setup() {
  Serial.begin(115200);                   // initialize serial
  while (!Serial);
  LoRa.setPins(nss, rst, dio0);
  Serial.println("LoRa WSN Gateway");
  if (!LoRa.begin(433E6)) {             // initialize ratio at 433 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
  Serial.println();
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(2000);
    Serial.println("Connecting..");
  }
  Serial.println("Connected..");
}

void loop() {
  //  Serial.println(millis());
  if (millis() - lastSendTime > interval) {
    // Observe the tank

    lastSendTime = millis();            // timestamp the message
    interval = 3000;    // 1 secs
    Serial.print("Next on: ");
    Serial.print(interval / 1000);
    Serial.println(" Sec");
    bacaweb();

  }
  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());

}

void sendCommand(void) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(packetLength);             // add payload length
  LoRa.write(aType1);                   // add payload
  LoRa.write(aArg1);
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  recipient = LoRa.read();         // recipient address
  sender = LoRa.read();            // sender address
  inMsgId = LoRa.read();           // incoming msg ID
  inLength = LoRa.read();          // incoming msg length
  inSType1 = LoRa.read();          // incoming payload
  inSValue1 = LoRa.read();
  inSType2 = LoRa.read();
  inSValue2 = LoRa.read();
  inSType3 = LoRa.read();
  inSValue3 = LoRa.read();
  inSType4 = LoRa.read();
  inSValue4 = LoRa.read();
  inSTipe = LoRa.read();

  //It will be used only if the payload can be packed in a incoming
  /*  String incoming = "";

    while (LoRa.available()) {
      incoming += (char)LoRa.read();
    }

    if (incomingLength != incoming.length()) {   // check length for error
      Serial.println("error: message length does not match length");
      return;                             // skip rest of function
    }
  */

  // if the recipient isn't this device or broadcast,
  if (recipient == localAddress || recipient == 0xFF)
  {

    Serial.print("Received from: 0x" + String(sender, HEX));
    Serial.println("  Sent to: 0x" + String(recipient, HEX));
    Serial.print("Message ID: " + String(inMsgId));
    Serial.println("  Message length: " + String(inLength));
    Serial.print("  S1 Type: " + String(inSType1));
    Serial.println("  S1 Value: " + String(inSValue1));
    Serial.print("  S2 Type: " + String(inSType2));
    Serial.println("  S2 Value: " + String(inSValue2));
    Serial.print("  S3 Type: " + String(inSType3));
    Serial.println("  S3 Value: " + String(inSValue3));
    Serial.print("  S4 Type: " + String(inSType4));
    Serial.println("  S4 Value: " + String(inSValue4));
    Serial.print("RSSI: " + String(LoRa.packetRssi()));
    Serial.println("  SNR: " + String(LoRa.packetSnr()));
    Serial.println("  Tipe: " + String(inSTipe));
    Serial.println();
    kirimdata();
  }
}

void kirimdata() {

  if (client.connect(server, 80)) {
    if (String(sender, HEX).substring(0, 1) == "1") {
      request_string = "http://jte.polnes.ac.id/public/lora/public/api/";
      request_string += "water?id_node=";
      request_string += String(sender, HEX);
      request_string += "&id_message=";
      request_string += String(inMsgId);
      request_string += "&battery=";
      request_string += String(inSValue1);
      request_string += "&valve=";
      request_string += String(inSValue2);
      request_string += "&level=";
      request_string += String(inSValue3);
      request_string += "&consumption=";
      request_string += String(inSValue4);
    }

    if (String(sender, HEX).substring(0, 1) == "3") {
      request_string = "http://jte.polnes.ac.id/public/lora/public/api/";
      request_string += "power?id_node=";
      request_string += String(sender, HEX);
      request_string += "&id_message=";
      request_string += String(inMsgId);
      request_string += "&fasa_r=";
      request_string += String(inSValue1);
      request_string += "&fasa_s=";
      request_string += String(inSValue2);
      request_string += "&fasa_t=";
      request_string += String(inSValue3);
      request_string += "&tiga_fasa=";
      request_string += String(inSValue4);
    }

    http.begin(request_string);
    http.POST(request_string);
    http.end();
    //      delay(5000);
    Serial.print("node : " + String(sender, HEX).substring(0, 1) + " ");
    Serial.print("data yang dikirim : " + WiFi.macAddress() + " Link = ");
    Serial.println(request_string);

    //      Serial.println(out);

  }
  bacaweb();
}
void bacaweb() {
  if (client.connect(server, 80)) {
    request_string = WebAddress;
    request_string += "controlindex/1";
    http.begin(request_string);
    http.GET();
    char json[500];
    String payload = http.getString();
    payload.toCharArray(json, 500);
    StaticJsonDocument<400> doc;
    deserializeJson(doc, json);

    int id_node = doc["id_node"];
    int sn_mode = doc["mode"];
    int valve1 = doc["valve1"];
    int valve2 = doc["valve2"];
    int valve3 = doc["valve3"];
    int valve4 = doc["valve4"];
    int valve5 = doc["valve5"];
    int valve6 = doc["valve6"];
    int valve7 = doc["valve7"];
    int valve8 = doc["valve8"];
    int valve9 = doc["valve9"];
    int valve10 = doc["valve10"];
    String updated_at = doc["updated_at"];

    Serial.print("mode : ");
    Serial.println(sn_mode);
    Serial.print("valve1 : ");
    Serial.println(valve1);
    Serial.print("valve2 : ");
    Serial.println(valve2);
    Serial.print("valve3 : ");
    Serial.println(valve3);
    Serial.print("valve4 : ");
    Serial.println(valve4);
    Serial.print("valve5 : ");
    Serial.println(valve5);
    Serial.print("valve6 : ");
    Serial.println(valve7);
    Serial.print("valve7 : ");
    Serial.println(valve7);
    Serial.print("valve8 : ");
    Serial.println(valve8);
    Serial.print("valve9 : ");
    Serial.println(valve9);
    Serial.print("valve10 : ");
    Serial.println(valve10);

    Serial.print("updated_at : ");
    Serial.println(updated_at);

    if (sn_mode == 1) {
      Serial.println("auto");
    }
    else {
      Serial.println("manual");
    }

    if ( String(sender, HEX) == "11" ) {
      if ( valve1 == 1 ) {
        c = 'A';
        Serial.println(c);
      }
      else if ( valve1 == 0 ) {
        c = 'B';
        Serial.println(c);
      }
    }

    if ( String(sender, HEX) == "12" ) {
      if ( valve2 == 1 ) {
        c = 'C';
        Serial.println(c);
      }
      else if ( valve2 == 0 ) {
        c = 'D';
        Serial.println(c);
      }
    }

    if ( String(sender, HEX) == "13" ) {
      if ( valve3 == 1 ) {
        c = 'E';
        Serial.println(c);
      }
      else if ( valve3 == 0 ) {
        c = 'F';
        Serial.println(c);
      }
    }

    if ( String(sender, HEX) == "14" ) {
      if ( valve4 == 1 ) {
        c = 'G';
        Serial.println(c);
      }
      else if ( valve4 == 0 ) {
        c = 'H';
        Serial.println(c);
      }
    }

    if ( String(sender, HEX) == "15" ) {
      if ( valve5 == 1 ) {
        c = 'I';
        Serial.println(c);
      }
      else if ( valve5 == 0 ) {
        c = 'J';
        Serial.println(c);
      }
    }

    if ( String(sender, HEX) == "16" ) {
      if ( valve6 == 1 ) {
        c = 'K';
        Serial.println(c);
      }
      else if ( valve6 == 0 ) {
        c = 'L';
        Serial.println(c);
      }
    }

    if ( c == 'A' ) {
      Serial.println("Open Valve at N1");
      Serial.println();
      destination = 0x11;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'B' ) {
      Serial.println(F("Close valve at N1"));
      Serial.println();
      destination = 0x11;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'C' ) {
      Serial.println(F("Open Valve at N2"));
      Serial.println();
      destination = 0x12;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'D' ) {
      Serial.println(F("Close valve at N2"));
      Serial.println();
      destination = 0x12;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'E' ) {
      Serial.println(F("Open Valve at N3"));
      Serial.println();
      destination = 0x13;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'F' ) {
      Serial.println(F("Close valve at N3"));
      Serial.println();
      destination = 0x13;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'G' ) {
      Serial.println(F("Open Valve at N4"));
      Serial.println();
      destination = 0x14;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'H' ) {
      Serial.println(F("Close valve at N4"));
      Serial.println();
      destination = 0x14;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'I' ) {
      Serial.println(F("Open Valve at N5"));
      Serial.println();
      destination = 0x15;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'J' ) {
      Serial.println(F("Close valve at N5"));
      Serial.println();
      destination = 0x15;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'K' ) {
      Serial.println(F("Open Valve at N6"));
      Serial.println();
      destination = 0x16;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'L' ) {
      Serial.println(F("Close valve at N6"));
      Serial.println();
      destination = 0x16;      // destination to send to
      aType1 = 0x03;           // valve
      aArg1 = 0x03;            // turn valve close
    }

    sendCommand();
    delay (10);

    http.end();

    Serial.print("data yang diminta : " + WiFi.macAddress() + " Link = ");
    Serial.println(request_string);
  }
}
