#include <SPI.h>
#include <LoRa.h>
#include <LiquidCrystal.h>

const int rs = 5, en = 4, d4 = 14, d5 = 15, d6 = 16, d7 = 17;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  Serial.begin(9600);
  while (!Serial);
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  // Print a message to the LCD.
  lcd.print("LoRa receiver !");  

  Serial.println("LoRa Receiver");

  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  // try to parse packet
  
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("Received packet '");
    lcd.setCursor(0,1);
    lcd.print("Received packet "
    );

    // read packet
    while (LoRa.available()) {
      char Paket = LoRa.read();
      Serial.print(Paket);
      lcd.setCursor(0,2);
      lcd.print(Paket);      
    }

    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
    lcd.setCursor(0,3);
    lcd.print(" with RSSI ");     
    lcd.print(LoRa.packetRssi()); 
    lcd.print("     ");       
  }
}
