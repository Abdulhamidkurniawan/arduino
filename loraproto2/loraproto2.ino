

/*
  LoRa Duplex communication

  Sends a message every half second, and polls continually
  for new incoming messages. Implements a one-byte addressing scheme,
  with 0xFF as the broadcast address.

  Uses readString() from Stream class to read payload. The Stream class'
  timeout may affect other functuons, like the radio's callback. For an

  created 28 April 2017
  by Tom Igoe
*/
#include <SPI.h>              // include libraries
#include <LoRa.h>
#include <SoftwareSerial.h>
#define DEBUG true
SoftwareSerial ser(4, 3); // RX, TX

String outgoing;              // outgoing message
String id_node,id_message,battery,valve,level,consumption;

byte msgCount = 0;            // count of outgoing messages
byte localAddress = 0x01;     // address of this device Gateway
byte destination = 0x11;      // destination to send to
byte packetLength = 2;        // packet length 
byte aType1 = 0x03;           // actuator type (valve)
byte aArg1 = 0x03;            // turn valve close
long lastSendTime = 0;        // last send time
int interval = 30000;         // first send after

void setup() {
  Serial.begin(115200);                   // initialize serial
  while (!Serial);

  ser.begin(115200);
  connectWifi();
  Serial.print("\nTCP/UDP Connection...\n");
  sendCommand("AT+CIPMUX=0\r\n",2000,DEBUG); // reset module
  delay(5000);


  // override the default CS, reset, and IRQ pins (optional)
  //LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin

  if (!LoRa.begin(433E6)) {             // initialize ratio at 433 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }
  Serial.println("\nLoRa WSN Gateway");
  Serial.println("\nLoRa init succeeded.");
  Serial.println();
}

void loop() {


if ( Serial.available()) {
    char c = toupper(Serial.read());
    if ( c == 'A' ){      
       Serial.println("Open Valve at N1");
       Serial.println();
       destination = 0x11;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'B' ){
       Serial.println(F("Close valve at N1")); 
       Serial.println();
       destination = 0x11;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'C' ){      
       Serial.println(F("Open Valve at N2"));
       Serial.println();
       destination = 0x12;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'D' ){
       Serial.println(F("Close valve at N2")); 
       Serial.println();
       destination = 0x12;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'E' ){      
       Serial.println(F("Open Valve at N3"));
       Serial.println();       
       destination = 0x13;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'F' ){
       Serial.println(F("Close valve at N3")); 
       Serial.println();           
       destination = 0x13;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'G' ){      
       Serial.println(F("Open Valve at N4"));
       Serial.println();
       destination = 0x14;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'H' ){
       Serial.println(F("Close valve at N4")); 
       Serial.println();
       destination = 0x14;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'I' ){      
       Serial.println(F("Open Valve at N5"));
       Serial.println();
       destination = 0x15;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'J' ){
       Serial.println(F("Close valve at N5")); 
       Serial.println();
       destination = 0x15;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'K' ){      
       Serial.println(F("Open Valve at N6"));
       Serial.println();       
       destination = 0x16;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'L' ){
       Serial.println(F("Close valve at N6")); 
       Serial.println();       
       destination = 0x16;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
  
    kirimCommand();
    delay (10);
  }

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
}

void kirimCommand(void) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(packetLength);             // add payload length
  LoRa.write(aType1);                   // add payload
  LoRa.write(aArg1);
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  byte recipient = LoRa.read();         // recipient address
  byte sender = LoRa.read();            // sender address
  byte inMsgId = LoRa.read();           // incoming msg ID
  byte inLength = LoRa.read();          // incoming msg length
  byte inSType1 = LoRa.read();          // incoming payload
  byte inSValue1 = LoRa.read();
  byte inSType2 = LoRa.read();    
  byte inSValue2 = LoRa.read(); 
  byte inSType3 = LoRa.read();    
  byte inSValue3 = LoRa.read(); 
  byte inSType4 = LoRa.read();    
  byte inSValue4 = LoRa.read();   

//It will be used only if the payload can be packed in a incoming
/*  String incoming = "";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }

  if (incomingLength != incoming.length()) {   // check length for error
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
  }
*/

  // if the recipient isn't this device or broadcast,
if (recipient == localAddress || recipient == 0xFF) 
 {

  Serial.print("Received from: 0x" + String(sender, HEX));
  Serial.println("  Sent to: 0x" + String(recipient, HEX));
  Serial.print("Message ID: " + String(inMsgId));
  Serial.println("  Message length: " + String(inLength));
  Serial.print("  S1 Type: " + String(inSType1)); 
  Serial.println("  S1 Value: " + String(inSValue1));
  Serial.print("  S2 Type: " + String(inSType2)); 
  Serial.println("  S2 Value: " + String(inSValue2));
  Serial.print("  S3 Type: " + String(inSType3)); 
  Serial.println("  S3 Value: " + String(inSValue3));      
  Serial.print("  S4 Type: " + String(inSType4)); 
  Serial.println("  S4 Value: " + String(inSValue4));  
  Serial.print("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("  SNR: " + String(LoRa.packetSnr()));
  Serial.println();
  }
  id_node = String(sender, HEX);
  id_message = String(inMsgId);
  battery = String(inSValue1);
  level = String(inSValue2);
  valve = String(inSValue3);
  consumption = String(inSValue4); 
  
  sendDataID("id_node="+id_node+"&id_message="+id_message+"&battery="+battery+"&valve="+valve+"&level="+level+"&consumption="+consumption);
}

void sendDataID(String id) {
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  
  cmd += "192.168.1.50";
  cmd += "\",80\r\n";
  sendCommand(cmd,1000,DEBUG);
  delay(5000);
  
  String cmd2 = "POST /lora/public/api/water?"; // Link ke skrip PHP                    
  cmd2 += id;  
  cmd2 += " HTTP/1.1\r\n";
  cmd2 += "Host: 192.168.1.50\r\n\r\n\r\n"; // Host
  String pjg="AT+CIPSEND=";
  pjg += cmd2.length();
  pjg += "\r\n";
    
  sendCommand("AT+CIPCLOSE",1000,DEBUG);
  sendCommand(pjg,1000,DEBUG);
  delay(500);
  sendCommand(cmd2,1000,DEBUG);
  delay(2000);
}

String sendCommand(String command, const int timeout, boolean debug) {
  String response = "";
      
  ser.print(command); // send the read character to the esp8266
      
  long int time = millis();
      
  while( (time+timeout) > millis()) {
    while(ser.available()) {
      // The esp has data so display its output to the serial window 
      char z = ser.read(); // read the next character.
      response+=z;
    }  
  }
      
  if(debug) {
    Serial.print(response);
  }
      
  return response;
}

void connectWifi() {
  //Set-wifi
  Serial.print("Restart Module...\n");
  sendCommand("AT+RST\r\n",2000,DEBUG); // reset module
  delay(2000);
  Serial.print("Set wifi mode : STA...\n");
  sendCommand("AT+CWMODE=1\r\n",1000,DEBUG); // configure as access point
  delay(2000);
  Serial.print("Connect to access point...\n");
  sendCommand("AT+CWJAP=\"LR\",\"passpass\"\r\n",3000,DEBUG);
  delay(5000);
  Serial.print("Check IP Address...\n");
  sendCommand("AT+CIFSR\r\n",1000,DEBUG); // get ip address
  delay(2000);  
}
