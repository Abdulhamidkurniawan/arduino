#include <SoftwareSerial.h>
#include <NewPing.h>
#include <LowPower.h>

#define DEBUG true
#define TRIGGER_PIN  7  // jumper pin TRIG sensor ke pin 12 arduino
#define ECHO_PIN     6  // jumper pin ECHO sensor ke pin 11 arduino
#define MAX_DISTANCE 200 // jarak maks (cm).

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // bikin class baru
SoftwareSerial ser(11,10); // TX, RX
unsigned int uS = 0; // kirim ping dan simpan hasilnya di variabel uS (satuannya mikrodetik)
unsigned int ketinggian = 0;                     //The id you've placed on the table you've created on the database
unsigned int temperatur = 0;             //The password for that id
String api = "QXIM6BXKA9R7VJF0";
String url = "";
int sensor_value = 0;
int LED1 = 2;

void setup() {
  // put your setup code here, to run once:
//  delay(2000);        
  pinMode(LED1, OUTPUT);       
  Serial.begin(115200);
  ser.begin(115200);
  connectWifi();
  Serial.print("TCP/UDP Connection...\n");
  sendCommand("AT+CIPMUX=0\r\n",2000,DEBUG); // reset module
  delay(2000);
}

void loop() {
  digitalWrite(LED1,HIGH);
  // put your main code here, to run repeatedly:
  sensor_value = analogRead(0);       //Read the sendor value connected on A0 
  uS = sonar.ping(); // kirim ping dan simpan hasilnya di variabel uS (satuannya mikrodetik)
  ketinggian = uS / US_ROUNDTRIP_CM;
  sendDataID();
  digitalWrite(LED1,LOW);
  delay(600000);
  tidur();

}

void sendDataID() {
  
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "api.thingspeak.com";
  cmd += "\",80\r\n";
  sendCommand(cmd,1000,DEBUG);
  delay(500);
  
  String cmd2 = "GET /update?api_key="; // Link ke skrip PHP                    

  cmd2 += api;  
  cmd2 += "&field1=";
  cmd2 += ketinggian;  
  cmd2 += "&field2=";
  cmd2 += random(30, 45);//sensor value
  cmd2 += " HTTP/1.1\r\n";
  cmd2 += "Host: api.thingspeak.com\r\n\r\n\r\n"; // Host

  String pjg="AT+CIPSEND=";
  pjg += cmd2.length();
  pjg += "\r\n";
    
  sendCommand(pjg,1000,DEBUG);
  delay(500);
  sendCommand(cmd2,1000,DEBUG);
  delay(1000);
//  tidur();
}

String sendCommand(String command, const int timeout, boolean debug) {
  String response = "";
  char ch; // No need to recreate the variable in a loop
      
  ser.print(command); // send the read character to the esp8266
//  Serial.print(command+"\n");
  long int time = millis();   

  while( (time+timeout) > millis()) {
    while(ser.available()) {
      // The esp has data so display its output to the serial window 
      char c = ser.read(); // read the next character.
      response+=c;
    }  
  }
      
  if(debug) {
    Serial.print(response);
  }
      
  return response;

}

void connectWifi() {
  //Set-wifi
    String response = "";
  char ch; // No need to recreate the variable in a loop
  Serial.print("Restart Module...\n");
  sendCommand("AT+RST\r\n",2000,DEBUG); // reset module
    while (Serial.available() > 0) {
  ch = Serial.read();
  response = response.concat(ch);
  Serial.print(response);
  }
  Serial.print(response);
  delay(1000);
  Serial.print("\nSet wifi mode : STA...\n");
  sendCommand("AT+CWMODE=1\r\n",1000,DEBUG); // configure as access point
  delay(2000);
  Serial.print("\nConnect to access point...\n");
  sendCommand("AT+CWJAP=\"SUNAR\",\"sanggrahan\"\r\n",3000,DEBUG);
  delay(1000);
  Serial.print("\nCheck IP Address...\n");
  sendCommand("AT+CIFSR\r\n",1000,DEBUG); // get ip address
  delay(1000);

}

void tidur(){
  delay(1000);
  Serial.println("Arduino:- Sleep");
  delay(200);
  digitalWrite(LED_BUILTIN,LOW);
  LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, 
                 SPI_OFF, USART0_OFF, TWI_OFF);
  Serial.println("Arduino:- Woke up");
  Serial.println("");
  }
