#include "DHT.h"
#define DHTPIN D2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
int led1 = D1;

void setup() {
  Serial.begin(9600);
  dht.begin();
  pinMode(led1, OUTPUT);
}
void loop() {
  digitalWrite(led1, HIGH);  // turn the LED on (HIGH is the voltage level)
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print("\n");
  digitalWrite(led1, LOW);  // turn the LED on (HIGH is the voltage level)
  delay(2000);
}