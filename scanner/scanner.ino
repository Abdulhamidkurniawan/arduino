#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial ECU(10, 11); // RX, TX (terhubung ke ECU)

const unsigned long ECU_BAUDRATE = 16040; // Sesuaikan dengan baud rate yang digunakan ECU

void setup() {
  Serial.begin(115200);        // Untuk komunikasi dengan Serial Monitor
  ECU.begin(ECU_BAUDRATE);     // Mulai komunikasi serial dengan ECU Yamaha
  
  // Menunggu beberapa saat untuk memastikan komunikasi berjalan
  delay(1000);
  
  Serial.println("Memulai pemindaian ECU Yamaha...");
  
  // Kirimkan perintah ke ECU (contoh perintah untuk mendapatkan data)
  ECU.write(0x01);  // Contoh perintah untuk meminta data dari ECU (perintah umum)
  delay(500);
}

void loop() {
  // Periksa jika ada data yang tersedia dari ECU
  if (ECU.available() > 0) {
    byte receivedByte = ECU.read();  // Baca byte data yang diterima dari ECU
    
    // Tampilkan data dalam format hexadecimal di Serial Monitor
    Serial.print("Data diterima dari ECU (Hex): ");
    Serial.println(receivedByte, HEX);
  }
  else {
    Serial.println("Tidak ada data dari ECU.");
  }
  
  delay(500);  // Tunggu sebentar sebelum memeriksa lagi
}
