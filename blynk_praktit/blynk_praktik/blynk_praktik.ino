#define BLYNK_PRINT Serial
#define BLYNK_TEMPLATE_ID "isi sendiri"
#define BLYNK_TEMPLATE_NAME "isi sendiri"
#define BLYNK_AUTH_TOKEN "isi sendiri"
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>


char auth[] = BLYNK_AUTH_TOKEN;
char ssid[] = "BL";   //Enter your WIFI name
char pass[] = "blblblbl";  //Enter your WIFI password

BlynkTimer timer;          // Creating a timer object

//Get the button value
BLYNK_WRITE(V0) {
  int pinValue = param.asInt();
  Serial.print("Button V0 : ");
  Serial.println(pinValue);
  digitalWrite(LED_BUILTIN, pinValue);  // turn the LED on (HIGH is the voltage level)
}

void myTimerEvent() {
  unsigned long seconds = millis() / 1000;
  Blynk.virtualWrite(V1, String(seconds) + " Detik");
//  Blynk.virtualWrite(V2, fasa_r);
//  Blynk.virtualWrite(V3, fasa_s);
//  Blynk.virtualWrite(V4, fasa_t);
//  Blynk.virtualWrite(V5, rst);
}

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  //Set the LED pin as an output pin
  pinMode(D0, OUTPUT);
  //Initialize the Blynk library
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(1000L, myTimerEvent);

}

void loop() {
  //Run the Blynk library
  Blynk.run();
  timer.run();

}
