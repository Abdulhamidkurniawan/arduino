// ---------------------------------------------------------------------------
// contoh program ukur jarak dg sensor ultrasonik
// saptaji.com 
//---------------------------------------------------------------------------

#include <NewPing.h>

#define TRIGGER_PIN  7  // jumper pin TRIG sensor ke pin 12 arduino
#define ECHO_PIN     6  // jumper pin ECHO sensor ke pin 11 arduino
#define MAX_DISTANCE 200 // jarak maks (cm).

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // bikin class baru

void setup() {
  Serial.begin(9600); // buka serial monitor dg  br 9600
}

void loop() {
  delay(50);                      // delay tiap pengukuran (bisa diset sdr)
  unsigned int uS = sonar.ping(); // kirim ping dan simpan hasilnya di variabel uS (satuannya mikrodetik)
  unsigned int cm = uS / US_ROUNDTRIP_CM;
  unsigned int persen = cm/2;
  Serial.print("Ping: "); //kirim tulisan 'Ping' ke serial monitor
  Serial.print(cm); // konversi pingtime ke jarak (cm)
  Serial.print("cm, ");   //kirim tulisan 'cm' ke serial monitor
  Serial.print(persen); // konversi pingtime ke jarak (cm)
  Serial.println("%");   //kirim tulisan 'cm' ke serial monitor
  delay (5000);
}
