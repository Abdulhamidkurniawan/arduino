#include <LoRa.h>

#define nss D10
#define rst D14
#define dio0 D2

void setup() {
  Serial.begin(115200);
  while (!Serial);
  LoRa.setPins(nss, rst, dio0);  
  Serial.println("LoRa Receiver");

  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    
    // received a packet
    Serial.print("Received packet :");

    // read packet
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }
   Serial.println("");
  }
}
