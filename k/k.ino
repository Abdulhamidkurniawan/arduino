#include <SoftwareSerial.h>
SoftwareSerial wifi (10,11);

const char* server = "192.168.1.107";
const char* ssid = "Kos";
const char* password = "kosmutiara18";
const char* SensorID = "ESP8266";

WiFiClient client;

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, HIGH);

  WiFi.begin(ssid, password);
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.print(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi Connected");
}

void loop() {
float n = random(300);

Serial.print("Random Number: ");
Serial.println(n);

  if (client.connect(server,80)) {
    Serial.print("Posting data...");
    digitalWrite(BUILTIN_LED, LOW);
    Serial.println("Random Number: " + String(n));
    client.println("GET /log.php?n=" + String(n));
    client.println("HOST: ");
    client.println(server);
    client.println("Connection: close");
    client.println();

    client.stop();
    Serial.println();

    digitalWrite(BUILTIN_LED, HIGH);
  }

  delay(5000);
