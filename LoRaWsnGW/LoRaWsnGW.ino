#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SPI.h>
#include <LoRa.h>

#define nss D10
#define rst D14
#define dio0 D2

WiFiClient client;
HTTPClient http;// Wi-Fi 

String ssid = "Lab_elektro";
String password = "";
String apikey = "QXIM6BXKA9R7VJF0";
String server = "jte.polnes.ac.id";

String WebAddress= "http://192.168.1.200/lora/public/api/water?";
String writeAPIKey;
String tsfield1Name;
String request_string;

String out;

String outgoing;              // outgoing message

byte msgCount = 0;            // count of outgoing messages
byte localAddress = 0x01;     // address of this device Gateway
byte destination = 0x11;      // destination to send to
byte packetLength = 2;        // packet length 
byte aType1 = 0x03;           // actuator type (valve)
byte aArg1 = 0x03;            // turn valve close

  byte recipient;         // recipient address
  byte sender;            // sender address
  byte inMsgId;           // incoming msg ID
  byte inLength;          // incoming msg length
  byte inSType1;          // incoming payload
  byte inSValue1;
  byte inSType2;    
  byte inSValue2; 
  byte inSType3;    
  byte inSValue3; 
  byte inSType4;    
  byte inSValue4;   


void setup() {
  Serial.begin(115200);                   // initialize serial
  while (!Serial);
  LoRa.setPins(nss, rst, dio0);  
  Serial.println("LoRa WSN Gateway");
    if (!LoRa.begin(433E6)) {             // initialize ratio at 433 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
  Serial.println();
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {  
    delay(2000);
    Serial.println("Connecting..");
  }
    Serial.println("Connected..");
}

void loop() {


if ( Serial.available()) {
    char c = toupper(Serial.read());
    if ( c == 'A' ){      
       Serial.println("Open Valve at N1");
       Serial.println();
       destination = 0x11;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'B' ){
       Serial.println(F("Close valve at N1")); 
       Serial.println();
       destination = 0x11;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'C' ){      
       Serial.println(F("Open Valve at N2"));
       Serial.println();
       destination = 0x12;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'D' ){
       Serial.println(F("Close valve at N2")); 
       Serial.println();
       destination = 0x12;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'E' ){      
       Serial.println(F("Open Valve at N3"));
       Serial.println();       
       destination = 0x13;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'F' ){
       Serial.println(F("Close valve at N3")); 
       Serial.println();           
       destination = 0x13;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'G' ){      
       Serial.println(F("Open Valve at N4"));
       Serial.println();
       destination = 0x14;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'H' ){
       Serial.println(F("Close valve at N4")); 
       Serial.println();
       destination = 0x14;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'I' ){      
       Serial.println(F("Open Valve at N5"));
       Serial.println();
       destination = 0x15;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'J' ){
       Serial.println(F("Close valve at N5")); 
       Serial.println();
       destination = 0x15;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
    else if ( c == 'K' ){      
       Serial.println(F("Open Valve at N6"));
       Serial.println();       
       destination = 0x16;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x00;            // turn valve open
    }
    else if ( c == 'L' ){
       Serial.println(F("Close valve at N6")); 
       Serial.println();       
       destination = 0x16;      // destination to send to
       aType1 = 0x03;           // valve
       aArg1 = 0x03;            // turn valve close
    }
  
    sendCommand();
    delay (10);
  }
  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
}

void sendCommand(void) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(packetLength);             // add payload length
  LoRa.write(aType1);                   // add payload
  LoRa.write(aArg1);
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  recipient = LoRa.read();         // recipient address
  sender = LoRa.read();            // sender address
  inMsgId = LoRa.read();           // incoming msg ID
  inLength = LoRa.read();          // incoming msg length
  inSType1 = LoRa.read();          // incoming payload
  inSValue1 = LoRa.read();
  inSType2 = LoRa.read();    
  inSValue2 = LoRa.read(); 
  inSType3 = LoRa.read();    
  inSValue3 = LoRa.read(); 
  inSType4 = LoRa.read();    
  inSValue4 = LoRa.read();   

//It will be used only if the payload can be packed in a incoming
/*  String incoming = "";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }

  if (incomingLength != incoming.length()) {   // check length for error
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
  }
*/

  // if the recipient isn't this device or broadcast,
if (recipient == localAddress || recipient == 0xFF) 
 {

  Serial.print("Received from: 0x" + String(sender, HEX));
  Serial.println("  Sent to: 0x" + String(recipient, HEX));
  Serial.print("Message ID: " + String(inMsgId));
  Serial.println("  Message length: " + String(inLength));
  Serial.print("  S1 Type: " + String(inSType1)); 
  Serial.println("  S1 Value: " + String(inSValue1));
  Serial.print("  S2 Type: " + String(inSType2)); 
  Serial.println("  S2 Value: " + String(inSValue2));
  Serial.print("  S3 Type: " + String(inSType3)); 
  Serial.println("  S3 Value: " + String(inSValue3));      
  Serial.print("  S4 Type: " + String(inSType4)); 
  Serial.println("  S4 Value: " + String(inSValue4));  
  Serial.print("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("  SNR: " + String(LoRa.packetSnr()));
  Serial.println();
  kirimdata();
  }
}

void kirimdata() {
 
if (client.connect(server,80)) {
      request_string = WebAddress;
      request_string += "id_node=";
      request_string += String(sender, HEX);
      request_string += "&id_message=";
      request_string += String(inMsgId);
      request_string += "&battery=";
      request_string += String(inSValue1);
      request_string += "&valve=";
      request_string += String(inSValue3);
      request_string += "&level=";
      request_string += String(inSValue2);
      request_string += "&consumption=";
      request_string += String(inSValue4);  // mengirim data sensor ke thingspeak.com
      http.begin(request_string);
      http.POST(request_string);
      http.end();
//      delay(5000);
      Serial.print("data yang dikirim : "+WiFi.macAddress()+" Link:");
      Serial.println(request_string);
      
//      Serial.println(out);

}
 
}
