#include <SoftwareSerial.h>
#define DEBUG true

SoftwareSerial ser(11,10); // TX, RX
String unit_id = "1";                     //The id you've placed on the table you've created on the database
String id_password = "12345";             //The password for that id
String url = "";
int sensor_value = 0;

void setup() {
  // put your setup code here, to run once:
//  delay(2000);               
  Serial.begin(115200);
  ser.begin(115200);
  connectWifi();
  Serial.print("TCP/UDP Connection...\n");
  sendCommand("AT+CIPMUX=0\r\n",2000,DEBUG); // reset module
  delay(2000);
}

void loop() {
  // put your main code here, to run repeatedly:
  sensor_value = analogRead(0);       //Read the sendor value connected on A0 
  sendDataID();
  delay(2000);
}

void sendDataID() {
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "bliot.000webhostapp.com";
  cmd += "\",80\r\n";
  sendCommand(cmd,1000,DEBUG);
  delay(1000);
  
  String cmd2 = "GET /iot2/rx.php?id="; // Link ke skrip PHP                    

  cmd2 += unit_id;  
  cmd2 += "&pw=";
  cmd2 +=  id_password;//sensor value
  cmd2 += "&s1=";
  cmd2 +=  String(sensor_value);//sensor value
  cmd2 += " HTTP/1.1\r\n";
  cmd2 += "Host: bliot.000webhostapp.com\r\n\r\n\r\n"; // Host

  String pjg="AT+CIPSEND=";
  pjg += cmd2.length();
  pjg += "\r\n";
    
  sendCommand(pjg,1000,DEBUG);
  delay(500);
  sendCommand(cmd2,1000,DEBUG);
  delay(1000);
}

String sendCommand(String command, const int timeout, boolean debug) {
  String response = "";
  char ch; // No need to recreate the variable in a loop
      
  ser.print(command); // send the read character to the esp8266
//  Serial.print(command+"\n");
  long int time = millis();   

  while( (time+timeout) > millis()) {
    while(ser.available()) {
      // The esp has data so display its output to the serial window 
      char c = ser.read(); // read the next character.
      response+=c;
    }  
  }
      
  if(debug) {
    Serial.print(response);
  }
      
  return response;
}

void connectWifi() {
  //Set-wifi
    String response = "";
  char ch; // No need to recreate the variable in a loop
  Serial.print("Restart Module...\n");
  sendCommand("AT+RST\r\n",2000,DEBUG); // reset module
    while (Serial.available() > 0) {
  ch = Serial.read();
  response = response.concat(ch);
  Serial.print(response);
  }
  Serial.print(response);
  delay(1000);
  Serial.print("\nSet wifi mode : STA...\n");
  sendCommand("AT+CWMODE=1\r\n",1000,DEBUG); // configure as access point
  delay(2000);
  Serial.print("\nConnect to access point...\n");
  sendCommand("AT+CWJAP=\"SUNAR\",\"sanggrahan\"\r\n",3000,DEBUG);
  delay(1000);
  Serial.print("\nCheck IP Address...\n");
  sendCommand("AT+CIFSR\r\n",1000,DEBUG); // get ip address
  delay(1000);

}
