#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

  int i=0;
  int analogInput0 = 0;
  int analogInput1 = 1;
  float vout0 = 0.0;
  float vout1 = 0.0;
  float cal0 = 0.0;
  float cal1 = 0.0;
  float vin0 = 0.0;
  float vin1 = 0.0;

  int val0 = 0;
  int val1 = 0;
  
void setup() {
//  analogReference (INTERNAL);
  lcd.begin();
  lcd.setCursor(0,0);
  lcd.print("TEST LCD i2C");
  lcd.setCursor(0,1);
  lcd.print("Samarinda");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Booting...");
  for(i=0;i<101;i++){
    lcd.setCursor(0,1);
  lcd.print(i);
  lcd.print(" %");
  delay(10);
    }
  lcd.clear();
  i=0;
}

void loop(){


  for(i=0;i<10;i++){
     // read the value at analog input
  val0 = analogRead(analogInput0);
  cal0 = val0*5.0/1023.0; 
  vin0 = readVcc() / 1000.0;
  vout0 = 1*(6*(vin0 / 5 * cal0));  
  
  val1 = analogRead(analogInput1);
  cal1 = val1*5.0/1023.0; 
  vin1 = readVcc() / 1000.0;
  vout1 = 1*(6*(vin1 / 5 * cal1));
  
  lcd.setCursor(0,0);
  lcd.print("FORW: ");
  lcd.print(vout0,1);
  lcd.print("w");
  lcd.setCursor(0,1);
  lcd.print("REFL: "); 
  lcd.print(vout1,1);
  lcd.print("w");
  delay(500);
  lcd.clear();
    }

  i=0;

}

long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result; // Calculate Vcc (in mV); 1126400 = 1.1*1024*1000
  return result;
}
