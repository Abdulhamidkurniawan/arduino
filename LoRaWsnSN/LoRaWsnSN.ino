

/*
  modified from LoRa Duplex communication  created 28 April 2017
  by Tom Igoe

  SN needs to listen whether they  receive a command from GW.
  Otherwise it sends data every period.
  0xFF is the broadcast address.

  Uses readString() from Stream class to read payload. The Stream class'
  timeout may affect other functuons, like the radio's callback. For an

  by Prihadi Murdiyat

*/
#include <SPI.h>              // include libraries
#include <LoRa.h>

int levelSwitch = 3;              // level switch
int valve = 4;                    // valve

byte msgCount = 0;            // count of outgoing messages
byte localAddress = 0x11;     // address of this device
byte destination = 0x01;      // Gateway address

//Data
byte packetLength = 8;      // payload length
byte sType1 = 0x01;         // sensor1 Type = Battery
byte sValue1 = 12;         // sensor1 Value = 12 V
byte sType2 = 0x02;         // sensor2 Type = Water level
byte sValue2 = 0x00;         // sensor2 Value = Empty
byte sType3 = 0x03;         // sensor3 Type = Valve status
byte sValue3 = 0x00;         // sensor3 Value = Open
byte sType4 = 0x04;         // sensor3 Type = water consumption
byte sValue4 = 00;         // sensor3 Value = 0 m3
byte sTipe = 00;         // Tipe Laporan



long lastSendTime = 0;        // last send time
int interval = 5000;          // interval between sends

void setup() {
  pinMode (levelSwitch, INPUT_PULLUP); //initialize level switch
  pinMode (valve, OUTPUT);      // initialize valve
  Serial.begin(115200);         // initialize serial
  while (!Serial);

  Serial.println("LoRa WSN Sensor Node");

  // override the default CS, reset, and IRQ pins (optional)
  //LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin

  if (!LoRa.begin(433E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
  Serial.println();
}

void loop() {

  if (millis() - lastSendTime > interval) {
    // Observe the tank
    if (digitalRead(levelSwitch) == LOW) { //level switch ON
      sValue3 = 0x00;  // empty
    }
    else {
      sValue3 = 0x03;  // full
    }

    // Check valve status
    if (digitalRead (valve) == LOW) { //Valve NC, close
      sValue2 = 0x03;  // close
    }
    else {
      sValue2 = 0x00;  // open
    }
    sTipe = 0x00;
    sendData();
    Serial.println("Sending data ");
    Serial.println(" ");
    lastSendTime = millis();            // timestamp the message
    interval = 20000;    // 10 secs
    Serial.print("Next on: ");
    Serial.print(interval / 1000);
    Serial.println(" Sec");
  }

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());

}

void sendData() {
  Serial.print("Sent to: 0x" + String(destination, HEX));
  Serial.println("  Sender: 0x" + String(localAddress, HEX));
  Serial.print("Message ID: " + String(msgCount));
  Serial.println("  Message length: " + String(packetLength));
  Serial.print("  S1 Type: " + String(sType1));
  Serial.println("  S1 Value: " + String(sValue1));
  Serial.print("  S2 Type: " + String(sType2));
  Serial.println("  S2 Value: " + String(sValue2));
  Serial.print("  S3 Type: " + String(sType3));
  Serial.println("  S3 Value: " + String(sValue3));
  Serial.print("  S4 Type: " + String(sType4));
  Serial.println("  S4 Value: " + String(sValue4));
  Serial.print("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("  SNR: " + String(LoRa.packetSnr()));
  Serial.print("Tipe Report :");
  Serial.println(sTipe);
  
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(packetLength);             // add payload length
  LoRa.write(sType1);
  LoRa.write(sValue1);
  LoRa.write(sType2);
  LoRa.write(sValue2);
  LoRa.write(sType3);
  LoRa.write(sValue3);
  LoRa.write(sType4);
  LoRa.write(sValue4);
  LoRa.write(sTipe);
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  byte recipient = LoRa.read();         // recipient address
  byte sender = LoRa.read();            // sender address
  byte inMsgId = LoRa.read();           // incoming msg ID
  byte inLength = LoRa.read();          // incoming msg length
  byte inAType1 = LoRa.read();          // incoming payload (Command Type)
  byte inAArg1 = LoRa.read();           // incoming payload (Command Argument)

  Serial.println("Pesan Response SN");
  Serial.println(inAArg1);
  Serial.println(sValue2);
  Serial.println(digitalRead(levelSwitch));

  // if the recipient is this device or broadcast,
  if (inAArg1 != sValue2) {

    if (recipient == localAddress || recipient == 0xFF)
    {
      //do the command and send a response
      if (String(inAArg1) == "0")
      {
        digitalWrite(valve, HIGH);  // open valve
        sValue2 = 0x00;           // change valve status to OPEN
        sTipe = 0x01;
        sendData();
        Serial.println("OPEN valve, Send ACK ");
        Serial.println();
      }
      else
      {
        digitalWrite(valve, LOW);    //close valve
        sValue2 = 0x03;            //change valve status to CLOSE
        sTipe = 0x01;
        sendData();
        Serial.println("CLOSE valve, Open ACK ");
        Serial.println();
      }
    }
  }
}
