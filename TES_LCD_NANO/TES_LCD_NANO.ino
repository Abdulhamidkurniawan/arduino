void setup()
{
  Serial.begin(9600);
  analogReference(DEFAULT);
}

void loop()
{
  float v = (float) 0.0047 * analogRead(A0) + 0.0256;
  Serial.println(v, 2);
  delay(500);
}
