//EXAMPLE HTTP REQUEST
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
 
const char* ssid = "Lab_elektro";
const char* password = "";
int LED1 = D3;

void setup () {
  pinMode(LED1, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
 
    delay(1000);
    Serial.println("Connecting..");
 
  }      
 
}
 
void loop() {
 
  if (WiFi.status() == WL_CONNECTED) {
 
    HTTPClient http;
     Serial.println("Connected..");
http.begin("http://jte.polnes.ac.id/public/lora/public/api/controlindex/1");
  int httpCode = http.GET();

  if (httpCode > 0) {
    char json[500];
    String payload = http.getString();
    payload.toCharArray(json, 500);
    StaticJsonDocument<400> doc;
    deserializeJson(doc, json);

    int id_node = doc["id_node"];
    int sn_mode = doc["mode"];
    int valve1 = doc["valve1"];
    int valve2 = doc["valve2"];
    int valve3 = doc["valve3"];
    int valve4 = doc["valve4"];
    int valve5 = doc["valve5"];
    int valve6 = doc["valve6"];
    int valve7 = doc["valve7"];
    int valve8 = doc["valve8"];
    int valve9 = doc["valve9"];
    int valve10 = doc["valve10"];
    String updated_at = doc["updated_at"];

    Serial.print("mode : ");
    Serial.println(sn_mode);
    Serial.print("valve1 : ");
    Serial.println(valve1);
    Serial.print("valve2 : ");
    Serial.println(valve2);
    Serial.print("valve3 : ");
    Serial.println(valve3);
    Serial.print("valve4 : ");
    Serial.println(valve4);
    Serial.print("valve5 : ");
    Serial.println(valve5);
    Serial.print("valve6 : ");
    Serial.println(valve7);
    Serial.print("valve7 : ");
    Serial.println(valve7);
    Serial.print("valve8 : ");
    Serial.println(valve8);
    Serial.print("valve9 : ");
    Serial.println(valve9);
    Serial.print("valve10 : ");
    Serial.println(valve10);

    Serial.print("updated_at : ");
    Serial.println(updated_at);

    if (sn_mode == 1) {
      Serial.println("auto");
    }
    else {
      Serial.println("manual");
    }
  }
 
    http.end();
 
  }

  delay(5000);
 
}
