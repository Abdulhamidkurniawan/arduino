#include <SoftwareSerial.h>
#define DEBUG true
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

SoftwareSerial ser(11, 10); // tx, rx

  int i=0;
  int analogInput0 = 0;
  int analogInput1 = 1;
  float vout0 = 0.0;
  float vout1 = 0.0;
  float cal0 = 0.0;
  float cal1 = 0.0;
  float vin0 = 0.0;
  float vin1 = 0.0;

  int val0 = 0;
  int val1 = 0;
  
void setup() {
  // put your setup code here, to run once:
  preStart();
  delay(2000);               
  Serial.begin(115200);
  ser.begin(115200);
  connectWifi();
  Serial.print("TCP/UDP Connection...\n");
  sendCommand("AT+CIPMUX=0\r\n",2000,DEBUG); // reset module
  delay(2000);
  lcd.clear();
}

void loop() {
  lcd.clear();
  voltMon();
  i=0;  
  sendDataID("data="+String(vout0));
}

void sendDataID(String id) {
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  
  cmd += "192.168.1.220";
  cmd += "\",80\r\n";
  sendCommand(cmd,1000,DEBUG);
  delay(5000);
  
  String cmd2 = "POST /lr/rx.php?"; // Link ke skrip PHP                    
  cmd2 += id;  
  cmd2 += " HTTP/1.1\r\n";
  cmd2 += "Host: 192.168.1.220\r\n\r\n\r\n"; // Host
  String pjg="AT+CIPSEND=";
  pjg += cmd2.length();
  pjg += "\r\n";
    
  sendCommand(pjg,1000,DEBUG);
  delay(500);
  sendCommand(cmd2,1000,DEBUG);
  delay(2000);
}

String sendCommand(String command, const int timeout, boolean debug) {
  String response = "";
      
  ser.print(command); // send the read character to the esp8266
      
  long int time = millis();
      
  while( (time+timeout) > millis()) {
    while(ser.available()) {
      // The esp has data so display its output to the serial window 
      char c = ser.read(); // read the next character.
      response+=c;
    }  
  }
      
  if(debug) {
    Serial.print(response);
  }
      
  return response;
}

void connectWifi() {
  //Set-wifi
  lcd.begin();
  Serial.print("Start Module...\n");
  lcd.setCursor(0,0);
  lcd.print("Start Module...");
  sendCommand("AT+RST\r\n",2000,DEBUG); // reset module
  delay(2000);
  Serial.print("Set wifi mode : STA...\n");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Set wifi mode");
  sendCommand("AT+CWMODE=1\r\n",1000,DEBUG); // configure as access point
  delay(2000);
  Serial.print("Connect to access point...\n");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Connect to wifi");
  sendCommand("AT+CWJAP=\"BL\",\"pelangi280293\"\r\n",3000,DEBUG);
  delay(5000);
  Serial.print("Check IP Address...\n");
   lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Check IP Address...");
  sendCommand("AT+CIFSR\r\n",1000,DEBUG); // get ip address
  delay(2000);  
}

void voltMon(){
    // put your main code here, to run repeatedly:
  for(i=0;i<10;i++){
     // read the value at analog input
  val0 = analogRead(analogInput0);
  cal0 = val0*5.0/1023.0; 
  vin0 = readVcc() / 1000.0;
  vout0 = 200*(6*(vin0 / 5 * cal0));  
  
  val1 = analogRead(analogInput1);
  cal1 = val1*5.0/1023.0; 
  vin1 = readVcc() / 1000.0;
  vout1 = 200*(6*(vin1 / 5 * cal1));
  
  lcd.setCursor(0,0);
  lcd.print("FORW: ");
  lcd.print(vout0,1);
  lcd.print("w");
  lcd.setCursor(0,1);
  lcd.print("REFL: "); 
  lcd.print(vout1,1);
  lcd.print("w");
  delay(500);
    }
  }

void preStart(){
  lcd.begin();
  lcd.setCursor(0,0);
  lcd.print("Booting...");
  for(i=0;i<101;i++){
  lcd.setCursor(0,1);
  lcd.print(i);
  lcd.print(" %");
  delay(10);
  }
  lcd.clear();
  }


long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result; // Calculate Vcc (in mV); 1126400 = 1.1*1024*1000
  return result;
}
