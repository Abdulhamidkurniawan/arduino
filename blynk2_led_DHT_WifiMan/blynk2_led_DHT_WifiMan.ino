#define BLYNK_PRINT Serial
#define BLYNK_TEMPLATE_ID "TMPLfGQO3XkN"
#define BLYNK_TEMPLATE_NAME "IoT DHT"
#define BLYNK_AUTH_TOKEN "43CJbREbod1EBCq_7WfwUy1i4zXyIQd6"
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <DHT.h>
#include <WiFiManager.h>

DHT dht(D2, DHT11);  //Pin, Jenis DHT

char auth[] = BLYNK_AUTH_TOKEN;
char ssid[] = "TssNode";   //Enter your WIFI name
char pass[] = "12345678";  //Enter your WIFI password
BlynkTimer timer;          // Creating a timer object
int LED = D1;
unsigned long previousMillis = 0;
unsigned long interval = 30000;
//Get the button value
BLYNK_WRITE(V0) {
  int pinValue = param.asInt();
  Serial.print("Button V0 : ");
  Serial.println(pinValue);
  digitalWrite(LED_BUILTIN, pinValue);  // turn the LED on (HIGH is the voltage level)
}

BLYNK_WRITE(V1) {
  int pinValue = param.asInt();
  Serial.print("Button V1 : ");
  Serial.println(pinValue);
  digitalWrite(LED, pinValue);  // turn the LED on (HIGH is the voltage level)
}

BLYNK_WRITE(V7) {
  int pinValue = param.asInt();
  Serial.print("Reset : ");
  Serial.println(pinValue);
  if (pinValue == 1) {
    delay(3000);
    ESP.restart();
    delay(5000);
  }  // turn the LED on (HIGH is the voltage level)
}

void myTimerEvent() {
  unsigned long currentMillis = millis();
  unsigned long seconds = currentMillis / 1000;
  unsigned long minutes = seconds / 60;
  unsigned long hours = minutes / 60;
  unsigned long days = hours / 24;
  currentMillis %= 1000;
  seconds %= 60;
  minutes %= 60;
  hours %= 24;

  Blynk.virtualWrite(V6, String(days) + " Day " + ":" + String(hours) + " Hour " + ":" + String(minutes) + " Min " + ":" + String(seconds) + " Sec");
  Blynk.virtualWrite(V7, 0);
  float kelembaban = dht.readHumidity();
  float suhu = dht.readTemperature();
  Blynk.virtualWrite(V3, suhu);
  Blynk.virtualWrite(V4, kelembaban);
  if (suhu >= 35) {
    digitalWrite(LED_BUILTIN, LOW);
    Blynk.virtualWrite(V5, 1);
    Blynk.virtualWrite(V0, 1);
    //Serial.print("kelembaban: ");
    //Serial.print(kelembaban);
    //Serial.print(" ");
    //Serial.print("suhu: ");
    //Serial.println(suhu);
  } else {
    digitalWrite(LED_BUILTIN, HIGH);
    Blynk.virtualWrite(V5, 0);
  }
  //  Serial.print("kelembaban: ");
  //  Serial.print(kelembaban);
  //  Serial.print(" ");
  //  Serial.print("suhu: ");
  //  Serial.println(suhu);
}

void setup() {
  WiFi.mode(WIFI_STA);  // explicitly set mode, esp defaults to STA+AP
  Serial.begin(9600);
  WiFiManager wm;
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, 1);
  pinMode(LED, OUTPUT);
  //Initialize the Blynk library
  dht.begin();
  timer.setInterval(1000L, myTimerEvent);
  bool res;
  res = wm.autoConnect(ssid, pass);  // password protected ap

  if (!res) {
    Serial.println("Failed to connect");
    // ESP.restart();
  } else {
    //if you get here you have connected to the WiFi
    WiFi.setAutoReconnect(true);
    WiFi.persistent(true);
    Serial.print("RSSI: ");
    Serial.println(WiFi.RSSI());
    Serial.println("connected...yeey :)");
    Blynk.config(auth);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.print("ESP Board MAC Address:  ");
    Serial.println(WiFi.macAddress());
  }
}

void loop() {
  //Run the Blynk library
  Blynk.run();
  timer.run();
  cek_wifi();
}

void cek_wifi() {
  //print the Wi-Fi status every 30 seconds
  unsigned long currentMillis1 = millis();
  if (currentMillis1 - previousMillis >= interval) {
    switch (WiFi.status()) {
      case WL_NO_SSID_AVAIL:
        Serial.println("Configured SSID cannot be reached");
        delay(3000);
        ESP.restart();
        delay(5000);
        break;
      case WL_CONNECTED:
        Serial.println("Connection successfully established");
        break;
      case WL_CONNECT_FAILED:
        Serial.println("Connection failed");
        delay(3000);
        ESP.restart();
        delay(5000);
        break;
    }
    Serial.printf("Connection status: %d\n", WiFi.status());
    Serial.print("RRSI: ");
    Serial.println(WiFi.RSSI());
    previousMillis = currentMillis1;
  }
}
