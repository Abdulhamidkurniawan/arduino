#include <SoftwareSerial.h>
#define DEBUG true

String ssid ="BL";
String password="pelangi280293";

SoftwareSerial ESP8266(11, 10); // RX, TX

unsigned long lastTimeMillis = 0;

void setup() {
  Serial.begin(115200);   
  ESP8266.begin(115200);
  reset();
  connectWifi();
  delay(2000);
}


//reset the esp8266 module

void reset() {

ESP8266.println("AT+RST");

delay(1000);

if(ESP8266.find("OK") ) Serial.println("Module Reset");

}


//connect to your wifi network
void connectWifi() {

String cmd = "AT+CWJAP=\"" +ssid+"\",\"" +password+ "\"";
ESP8266.println(cmd);

delay(4000);

if(ESP8266.find("OK")) {

Serial.println("Connected!");

}

else {

connectWifi();

Serial.println("Cannot connect to wifi"); }

}

void printResponse() {
  while (ESP8266.available()) {
    Serial.println(ESP8266.readStringUntil('\n'));
  }
}

void loop() {

  if (millis() - lastTimeMillis > 30000) {
    lastTimeMillis = millis();

    ESP8266.println("AT+CIPMUX=1");
    delay(1000);
    printResponse();



    ESP8266.println("AT+CIPSTART=4,\"TCP\",\"192.168.1.161\",80");
    delay(1000);
    printResponse();

    String cmd ="GET /ard/sensor.html HTTP/1.1\r\nUser-Agent: curl/7.37.0\r\nHost: %s\r\nAccept: */*\r\n\r\n";
    ESP8266.println("AT+CIPSEND=4," + String(cmd.length()));
    delay(1000);

    ESP8266.println(cmd);
    delay(1000);
    ESP8266.println("");
  }

  if (ESP8266.available()) {
    Serial.write(ESP8266.read());
  }

}
